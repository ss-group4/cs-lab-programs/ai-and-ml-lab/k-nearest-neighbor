import numpy as np
from collections import Counter


class KNN:
    def __init__(self, k=5):
        self.k = k

    def euclidean_distance(self, x1, x2):
        return np.sqrt(np.sum((x1 - x2) ** 2))

    def fit(self, X, y):
        self.X_train = X
        self.y_train = y

    def predict(self, X):
        y_pred = [self._predict(x) for x in X]
        return np.array(y_pred)

    def _predict(self, x):
        # Compute distances between x and all examples in the training set
        distances = [self.euclidean_distance(x, x_train) for x_train in self.X_train]

        # Get the k-nearest examples and their labels
        k_nearest = np.argsort(distances)[:self.k]
        k_nearest_labels = [self.y_train[i] for i in k_nearest]

        # Predict the label of the input by taking the majority vote of the k-nearest neighbors
        most_common = Counter(k_nearest_labels).most_common(1)
        return most_common[0][0]


from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from knn import KNN

# Load the Iris dataset
iris = load_iris()

# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2)

# Create a KNN object and fit it to the training data
knn = KNN(k=3)
knn.fit(X_train, y_train)

# Make predictions on the test data

y_pred = knn.predict(X_test)
print(y_pred)
# Calculate the accuracy of the predictions
accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy}")
