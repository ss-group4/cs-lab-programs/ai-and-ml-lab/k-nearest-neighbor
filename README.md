

# K Nearest neighbors
This contains demonstration for K nearest neighbor model.
## Algorithm
Step-1: Select the number K of the neighbors.

Step-2: Calculate the Euclidean distance/Manhattan of K number of neighbors.

Step-3: Take the K nearest neighbors as per the calculated Euclidean distance. Make sure that the distance between the Dataset and data to be predicted is
lesser than the K selection.

Step-4: Among these k neighbors, count the number of the data points in each category.

Step-5: Assign the new data points to that category for which the number of the neighbor is maximum.


