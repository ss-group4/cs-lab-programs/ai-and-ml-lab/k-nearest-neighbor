import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix


df = pd.read_csv('./mock_knn.csv')

print("Points for prediction: ")
# new_x = int(input("Enter new X1 "))
# new_y = int(input("Enter New X2 "))
# new_point = [[new_x, new_y]]
# x = [[10,20],[20,30],[40,50],[10,40],[10,22],[18,19]]
# y= [1,2,3,2,1,3]
clf = KNeighborsClassifier(3)
 # clf.fit(x,y)

# print(clf.predict(new_point))

x= list(df.iloc[:, 2:-1].values) # Ignore first User ID

y = list(df.iloc[:,-1].values)
print(x)
print(y)


x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.2)

clf.fit(x_train,y_train)
pred = clf.predict(x_test)
print(pred)

cfg = confusion_matrix(y_test,pred)
tn, fp, fn, tp = confusion_matrix(y_test,pred).ravel() # Split 3 value
print('(tn, fp, fn, tp)',(tn, fp, fn, tp))


